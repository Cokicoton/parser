<DOCTYPE! HTML>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <title>CMS Assistant</title>
</head>
<body>
<?php
include 'parser.php';
include_once 'tools.php';
include 'style.php';
include 'visuel.php';

$console = "";

if (isset($_GET['ftp'])) {
  if ($_GET['ftp'] == 'oui') {
    $commande = "./ftp.sh";
    exec($commande, $resultat);
  }
}

$fichier = $_GET["file"];

$regex = "/^[0-9]{4} [0-9]{2} [0-9]{2}\w*/";
if (preg_match($regex, $fichier)) {
  $temp = explode(" ", substr($fichier, 0, 10));
  $year = $temp[0];
  $month = $temp[1];
  $day = $temp[2];
  $date = $day . "/" . $month . "/" . $year;
} else {
  $date = "unknown";
}

if (!file_exists("/temp/word")) {
  //$commande1 = "copy " . str_replace(' ', '\ ', $fichier) . " " . str_replace('.docx', '.zip', str_replace(' ', '\ ', $fichier)); //windows
  //exec($commande1, $resultat); //windows
  $commande = "unzip ../document/" . str_replace(' ', '\ ', $fichier) . " -d temp"; //linux
  //$commande = 'c:/"Program Files"/7-Zip/7z.exe x c:/wamp64/www/parser_docx/' . str_replace('.docx', '.zip', str_replace(' ', '\ ', $fichier)) . " -oc:/wamp64/www/parser_docx/temp"; //windows
  exec($commande, $resultat);
  /*$taille_resultat = sizeof($resultat); // inutile affiche tous les exctracts
  for ($i = 0; $i < $taille_resultat; $i++){
    $console = $console . $resultat[$i] . "<br>\n";
  }*/
} else {
  $console = "Document existant...<br>\n";
}

$file=fopen("temp/word/document.xml", "r");

if ($file){
  $stat = 0;
  while (!feof($file))
	{ $stat++;
		$buffer = fgets($file); //Apparement le fichier document.xml contient deux chaines, une vide et l'autre contenant l'ensemble du fichier
	}
  fclose($file);

  //Décomposition du fichier en tableau de ligne xml
  $content_xml = body_separe($buffer); // Contient le xml interne au body
  $tab_xml = decompose($content_xml);
  $nb_lignes_xml = sizeof($tab_xml);

  $titre = "";
  $corps = "<div id='corps_texte'>\n";
  $liste_en_cours = false;
  $liste_type = "";
  $img_en_cours = false;
  $nb_images_tot = 0;

  for ($i=0; $i<$nb_lignes_xml; $i++) {

    $deb = style_word(decoupe($tab_xml[$i])[1])[0];
    $fin = style_word(decoupe($tab_xml[$i])[1])[1];
    $option = style_word(decoupe($tab_xml[$i])[1])[2];
    $text_content = decoupe($tab_xml[$i])[0];

    if (($deb == "no_balise") || (contient_image($tab_xml[$i]))) {
      if ($liste_en_cours) {
        if ($liste_type == "ul") {
          $corps = $corps . "</ul>\n\n";
        } elseif ($liste_type == "ol") {
          $corps = $corps . "</ol>\n\n";
        }
        $liste_en_cours = false;
      }
      if ($fin == "titre1") {
        if ($titre == "") {
          $titre = $text_content;
        } else {
          $corps = $corps . "<h3>" . $text_content . "</h3>" . "\n\n";
        }
      } elseif (($fin == "image") || (contient_image($tab_xml[$i]))) {
          $balises_image = explode('<wp:docPr id="', $tab_xml[$i]);
          $nb_image = sizeof($balises_image)-1;
          $tab_images = [];
          for($a=1; $a<=$nb_image; $a++) {
            $id = intval(explode('"', $balises_image[$a])[0]);error_log("id : " . $id . " init: " . $balises_image[$a] ."\n\n");
            $temp = explode('name="', $balises_image[$a])[1];error_log("image : " . $tab_xml[$i]);
            $temp2 = explode('"', $temp)[0];
            $nom_image = rename_image(strtolower($temp2), $nb_images_tot);
            $temp_img = '<figure>' . "\n";
            $temp_img = $temp_img . '  <img src="/images/' . $nom_image . '" class="images"/>' . "\n";
            $temp_img = $temp_img . '</figure>' . "\n";
            $tab_images[$id] =  $temp_img;
            $nb_images_tot++;
            $console = $console . "Ajout de l'image : " . $nom_image . "<br>\n";
          }error_log("nbimg : " . $nb_image . "\n\n");
          ksort($tab_images);
          foreach($tab_images as $key => $valeur){ //remise dans l'ordre des images par ID
            $corps = $corps . $valeur;
          }
          $corps = $corps . "</br>\n";
          $img_en_cours = true;
      }
    } else {
      if (contient($deb, "<li>")) {
        if ($liste_en_cours) { // une liste est en cours il ne s'agit que 'un nouvel element
          if ($option == $liste_type) {
            // rien à faire
          } else {
            $deb = "</" . $liste_type . ">\n\n<" . $option . ">" . $deb ;
            $liste_type = $option;
          }
          //rien à faire
        } else { // aucune liste est en cours, on la créé
          if ($option == "ul") {
            $deb = "<ul>" . $deb;
            $liste_type = "ul";
          } elseif ($option == "ol") {
            $deb = "<ol>" . $deb;
            $liste_type = "ol";
          }
          $liste_en_cours = true;
        }
      } else {
        if ($liste_en_cours) { // une liste est en cours mais il n'y a plus d'élement à y ajouter
          if ($liste_type == "ul") {
            //$fin = $fin . "</ul>\n\n";
            $deb = "</ul>\n\n" . $deb;
          } elseif ($liste_type == "ol") {
            //$fin = $fin . "</ol>\n\n";
            $deb = "</ol>\n\n" . $deb;
          }
          $liste_en_cours = false;
        }
      }

      if (contient($deb, "<legend>") && $img_en_cours){
        $temp1 = explode('</figure>', $corps);
        $temp2 = array_pop($temp1);
        $temp3 = array(implode('</figure>', $temp1), $temp2);
        $corps = $temp3[0] . '  <figcaption><center>' . $text_content .  '</center></figcaption>' . "\n";
        $corps = $corps . '</figure><br>' . "\n";
        $img_en_cours = false;
      } else {
      $corps = $corps . $deb . $text_content . $fin . "\n\n";
      }
    }
    if($fin != "image" && !contient_image($tab_xml[$i])){
      $img_en_cours = false;
    }
  }

  $console = $console . "Parsing terminé <br>\n";
}
$corps = str_replace('«', '«<em>', str_replace('»', '</em>»',$corps));
$titre = str_replace('«', '«<em>', str_replace('»', '</em>»',$titre));
$corps = $corps . "\n</div>";
if (creation_visuel($corps, $titre)) {
  $console = $console . "Création du visuel terminé<br>\n";
}
?>
<main id="corps">
  <?php
    echo '<input id="titre" type="text" value="' . $titre . '" placeholder="Titre de la page" />';
    echo '<textarea id="texte" type="textarea">' . $corps . '</textarea>';
  ?>
  <input id="slide" placeholder="Chemin de l'image pour le slide" type="text"/>
  <div id="icon">
    <img src="icon/copy.jpg" width="40px" id="copy" title="Copier le texte"/>
    <img src="icon/key_words.jpg" width="40px" id="key_words" title="Découper les mots-clés"/>
    <img src="icon/javascript.jpeg" width="40px" id="injection" title="Copier l'injection Javascript"/>
    <img src="icon/guttenberg.png" width="40px" id="guttenberg" title="Copier l'injection Javascript pour Guttenberg"/>
  </div>
</main>
<aside id="proprietes">
  <div id="date"><?php echo $date ?></div>
  <div id="console"><?php echo $console ?></div>
  <div id="ajouts">
    <input type="button" value="Ajouter version anglaise" onclick="ajouter_version('anglais');"/>
    <input type="button" value="Ajouter version allemande" onclick="ajouter_version('allemand');"/>
    <input type="button" value="Ajouter version catalane" onclick="ajouter_version('catalan');"/>
    <input type="button" value="Ajouter version espagnole" onclick="ajouter_version('espagnol');"/>
    <input type="button" value="Ajouter version portugaise" onclick="ajouter_version('portugais');"/>
    <input type="button" value="Ajouter version turque" onclick="ajouter_version('turk');"/>
    <div>
          <input type="button" value="Esperanto" onclick="ajouter_version('esperanto');"/>
          <input type="button" value="Italien" onclick="ajouter_version('italien');"/>
          <input type="button" value="Russe" onclick="ajouter_version('russe');"/>
    </div>
    <input type="button" value="Ajouter une autre version" onclick="ajouter_version('divers');"/>
    <hr>
    <input type="button" value="Ajouter un PDF" onclick="ajouter_pdf();"/>
    <hr>
    <input type="button" value="Ajouter balise more" onclick="ajouter_more()"/>
    <hr>
    <a href="visuel.html" target="_blank"><input type="button" value="Visualiser la page"/></a>
<!--    <hr>
    <a href="[URL_UPLOAD]" id="ftp" target="_blank"><input type="button" value="Uploader images/documents"/></a>-->
    <hr>
    <a href="download.php" target="_blank"><input type="button" value="Télécharger les images"/></a>
    <hr>
    <input type="button" value="Retour au choix du document" onclick="retour_accueil()"/>
  </div>
</aside>
 <script src="js/tools.js"></script>
 <script src="js/script.js"></script>
 <script src="js/edit.js"></script>
</body>
</html>
