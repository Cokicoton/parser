<?php

function body_separe($string){ // Retourne uniquement la partie du xml à l'intérieur des balises body
  $temp = explode("<w:body>", $string)[1];
  return explode("</w:body>", $temp)[0];
}

function decompose($string) { //décompose un xml en plusieurs lignes, retourne un tableau contenant chaque ligne
  $string =  $string . "?"; // pour la boucle qui doit s'arreter à l'avant dernier charactere
  $taille_chaine = strlen($string);
  $balise_ouvertes = 0; // nombre de balises ouvrantes ou fermantes ouvertes
  $balise_globales = 0; // nombre de balises ouvrantes non closes par une balises fermantes
  $tab_result = [];
  $chaine = "";
  for($i=0; $i<$taille_chaine-1; $i++){
    switch ($string[$i]) {
      case "<" : $balise_ouvertes++;
                 if ($string[$i+1] != "/") {
                   $balise_globales++;
                 } else {
                   $balise_globales--;
                 }
                 $chaine = $chaine . $string[$i];
                 break;
      case ">" : $balise_ouvertes--;
                 $chaine = $chaine . $string[$i];
                 if (($balise_ouvertes == 0) && ($balise_globales == 0)) {
                   array_push($tab_result, $chaine);
                   $chaine = "";
                 }
                break;
      case "/" : $chaine = $chaine . $string[$i];
                 if ($string[$i+1] == ">") {
                    $balise_ouvertes--;
                    $balise_globales--;
                    $i++;
                    $chaine = $chaine . $string[$i];
                 }
                break;
      default: $chaine = $chaine . $string[$i];
                break;
    }
  }
  return $tab_result;
}

function decoupe($string){ //separe les balises avant le contenu et le contenu lui-même renvoi un tablea avec ces 2 composantes
  $taille_chaine = strlen($string);
  $balises_ouvertes = 0;
  $chaine_en_cours = ""; //contenu du texte
  $fin_balises = 0;; //fin des balises avant le contenu
  for($i=0; $i<$taille_chaine; $i++){
    if ($string[$i] == "<") {
      $balises_ouvertes++;
    } elseif ($string[$i] == ">") {
      $balises_ouvertes--;
    } elseif ($balises_ouvertes == 0){
      if ($chaine_en_cours == "") {
        $fin_balises = $i;
      }
      $chaine_en_cours = $chaine_en_cours . $string[$i];
    }
  }
  $tab_resultats = [$chaine_en_cours, substr($string, 0, $fin_balises), substr($string, $fin_balises + strlen($chaine_en_cours), strlen($string))];
  return $tab_resultats;
}

?>
