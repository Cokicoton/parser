<?php
exec('cd /var/www/html/parser/images/;tar -czvf images.tar.gz *.jpg');
$attachment_location = '/var/www/html/parser/images/images.tar.gz';
if (file_exists($attachment_location)) {
    header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
    header("Cache-Control: public"); // needed for internet explorer
    header("Content-Type: application/tar+gzip");
    header("Content-Transfer-Encoding: Binary");
    header("Content-Length:".filesize($attachment_location));
    header("Content-Disposition: attachment; filename=images.tar.gz");
    readfile($attachment_location);
    die();
} else {
    die("Error: File not found.");
}
?>
