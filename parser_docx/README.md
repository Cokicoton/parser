<p>Modifier le fichier ftp.sh comme suit en modifiant les informations de connexion</p>
<code>
#!/bin/bash
cd images
ftp -i -n ftp.web4all.fr << END_SCRIPT
quote USER [userftp]
quote PASS [passworftp]
cd [urlsitewordpress]/htdocs/images
ls 5*
mput *
quit
END_SCRIPT
</code>
