#!/bin/bash
cd images
ftp -i -n [ftphost] << END_SCRIPT
quote USER [ftpuser]
quote PASS [ftppassword]
cd [ftpfolderpath]
ls 5*
mput *
quit
END_SCRIPT
