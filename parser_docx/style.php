<?php

include_once 'tools.php';

function style_word($string){ //prends des balises xml en entrée et ressort des balises html avec le style adéquat
  $result_debut = "";
  $result_fin = "";
  $result_option = "";
  if(contient($string, "Titre1")) {
    $tab = ["no_balise", "titre1", ""];
    return $tab;
  }
  if(contient($string, "<w:drawing>")) {
    $tab = ["no_balise", "image", ""];
    return $tab;
  }
  if (contient($string, "Titre2")) {
    $result_debut = $result_debut . "<h3>";
    $result_fin = "</h3>" . $result_fin;
  }
  if (contient($string, 'Citation"')) {
    $result_debut = $result_debut . "<quote class='citations'>";
    $result_fin = "</quote>" . $result_fin;
  }
  if (contient($string, 'Lgende')){
    $result_debut = $result_debut . "<legend>";
    $result_fin = "</legend>" . $result_fin;
  }
  if (contient($string, "center") && !contient($string, 'Lgende')) {
    $result_debut = $result_debut . "<center>";
    $result_fin = "</center>" . $result_fin;
  }
  if (contient($string, "<w:b/>")) {
    $result_debut = $result_debut . "<b>";
    $result_fin = "</b>" . $result_fin;
  }
  if (contient($string, "<w:i/>")) {
    $result_debut = $result_debut . "<em>";
    $result_fin = "</em>" . $result_fin;
  }
  if (contient($string, '<w:u w:val="single"/>')) {
    $result_debut = $result_debut . "<u>";
    $result_fin = "</u>" . $result_fin;
  }
  if (contient($string, '<w:numId w:val="1')) {
    $result_debut = $result_debut . "<li>";
    $result_fin = "</li>" . $result_fin;
    $result_option = "ul";
  }
  if (contient($string, '<w:numId w:val="2')) {
    $result_debut = $result_debut . "<li>";
    $result_fin = "</li>" . $result_fin;
    $result_option = "ol";
  }

  $tab = [$result_debut, $result_fin, $result_option];
  return $tab;
}

function contient_image($string) {
  if(contient($string, "<w:drawing>")) {
    return true;
  } else {
    return false;
  }
}
?>
