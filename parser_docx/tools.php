<?php

function contient($string, $motif){
  $temp = $string . "?"; //au cas où le motif est en fin de chaine
  if (isset(explode($motif, $temp)[1])) {
    return true;
  } else {
    return false;
  }
}

function rmAllDir($strDirectory){
    $handle = opendir($strDirectory);
    while(false !== ($entry = readdir($handle))){
        if($entry != '.' && $entry != '..'){
            if(is_dir($strDirectory.'/'.$entry)){
                rmAllDir($strDirectory.'/'.$entry);
            }
            elseif(is_file($strDirectory.'/'.$entry)){
                unlink($strDirectory.'/'.$entry);
            }
        }
    }
    rmdir($strDirectory.'/'.$entry);
    closedir($handle);
}

function find_image($string) {
  $liste_fichiers = scandir("temp/word/media");
  $taille_liste = sizeof($liste_fichiers);
  $temp = str_replace(' ', '', $string) . ".";

  for ($i=0; $i < $taille_liste; $i++) {
    if (isset(explode($temp, $liste_fichiers[$i])[1])) {
      return $liste_fichiers[$i];
    }
  }
}

function convertImage() {
  $file_path = '/var/www/html/parser/images/';
  foreach(scandir($file_path) as $file) {
    if(strpos($file, '.jpg') !== false){
      unset($output);
      exec('file --mime-type ' . $file_path . $file, $output);
      $file_type = explode(' ', $output[0])[1];

      switch ($file_type) {
        case 'image/png':
            exec('convert ' . $file_path . $file . ' ' . $file_path . $file);
            break;
        case 'image/gif':
            exec('convert ' . $file_path . $file . ' ' . $file_path . $file);
            break;
        case 'image/bmp':
            exec('convert ' . $file_path . $file . ' ' . $file_path . $file);
            break;
        case 'application/octet-stream':
            exec('libreoffice --headless --convert-to jpg ' . $file_path . $file);
            break;
      }
    }
    exec('convert -trim ' . $file_path . $file . ' ' . $file_path . $file);
  }
}

function rename_image($name, $nb_images_tot) { //$string = nom avec extension, $name = nom sans extension
  //$nom = str_replace(' ', '', $name);
  $nom = 'image' . strval($nb_images_tot + 1);
  $string = find_image($nom);
  $extension = explode($nom, $string)[1];
//  $nom_unique = uniqid() . $extension;
  $nom_unique = uniqid() . '.jpg';
  $commande = "cp temp/word/media/" . $string . " ../images/" . $nom_unique; //linux
  //$commande = 'copy temp\word\media\\' . $string . " images\\" . $nom_unique; //windows
  exec($commande, $resultat);
  convertImage();
  $commande = "cp temp/word/media/" . $string . " ../parser_docx/images/" . $nom_unique; //linux
  exec($commande, $resultat);
  return $nom_unique;
}
?>
