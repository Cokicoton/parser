<?php
include 'version.php';
?>
<DOCTYPE! HTML>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" type="text/css" href="css/accueil.css">
  <title>Parser <?php echo $version; ?></title>
  <link rel="icon" href="icon/parser.png" type="image/png"/>
  <link rel="shortcut icon" href="icon/parser.png" type="image/png"/>
</head>
<body>
<?php

include_once 'tools_commun.php';

if(is_dir("/var/www/html/parser/parser_docx/temp")){
  rmAllDir("/var/www/html/parser/parser_docx/temp");
}
if(is_dir("/var/www/html/parser/images")){
  rmAllDir("/var/www/html/parser/images");
}
if(is_dir("/var/www/html/parser/parser_docx/images")){
  rmAllDir("/var/www/html/parser/parser_docx/images");
}
if(is_dir("/var/www/html/parser/parser_odt/temp")){
  rmAllDir("/var/www/html/parser/parser_odt/temp");
}
if(is_dir("/var/www/html/parser/parser_odt/images")){
  rmAllDir("/var/www/html/parser/parser_odt/images");
}

if (!is_dir("temp")) {mkdir("temp");}
if (!is_dir("images")) {mkdir("images");}
if (!is_dir("parser_docx/images")) {mkdir("parser_docx/images");}

$liste_files = scandir('/var/www/html/parser/document');

$nb_files = sizeof($liste_files);

echo"<div id='window'>\n
      <div id='dossier'>\n";
for ($i=0; $i < $nb_files; $i++){

  if ((contient($liste_files[$i], '.docx') or contient($liste_files[$i], '.odt')) and !contient($liste_files[$i], '~')) { //seulement les docx et les .odt et en ignorant les fichiers en edition ~NOM.docx ou ~NOM.odt
    if(contient($liste_files[$i], '.docx')){
      echo '<div class="fichier docx" id="' .  $liste_files[$i] . '""><img src="icon/doc.png"  class="icon" /><p>' . explode('.docx', $liste_files[$i])[0] ."</p></div>\n";
    } elseif(contient($liste_files[$i], '.odt')) {
      echo '<div class="fichier odt" id="' .  $liste_files[$i] . '""><img src="icon/odt.png"  class="icon" /><p>' . explode('.odt', $liste_files[$i])[0] ."</p></div>\n";
    }
  }
}
echo "</div>\n";
echo "</div>\n";

?>
<input type="file" id="fileInput" enctype='multipart/form-data' />
<script src="js/tools.js"></script>
<script src="js/accueil.js"></script>
</body>
</html>
