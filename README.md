# parser

## Web app permettant de parser un fichier docx ou odt et convertir son contenu en html

![capture](parser_screenshot.png)

* Faire glisser les documents (docx ou odt) sur la page d'accueil pour les ajouter
* Sélectionner le document sur lequel travailler
* Le menu droit permet l'ajout de balise prédéfinie (more, différentes langues, pdf) et de prévisualiser le rendu
* les boutons en bas de l'écran permettent
* * La copie du corps du documents
* * La découpe de mots-clés au format wordpress
* * La copie sous forme de script javascript permettant l'ajout en article wordpress (Version avant Gutenberg)
* * La copie sous forme de script javascript permettant l'ajout en article wordpress (Version avec Gutenberg)


## Paramètrage
Modifier le lien vers les pdf dans les scripts \[LIEN_SITE+CHEMIN_PDF\]
* parser_docx/js/script.js
* parser_docx/js/visuel.js
* parser_odt/js/script.js
* parser_odt/js/visuel.js

Modifier le lien vers les fichiers uploadés \[URL_UPLOAD\]
* parser_docx/main.php
* parser_odt/main.php
