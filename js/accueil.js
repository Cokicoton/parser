var fichiers = document.querySelectorAll('.fichier');
var nb_fichiers = fichiers.length;
var i;
for(i=0; i<nb_fichiers; i++){
  fichiers[i].ondblclick = function(){
    if(this.classList.contains("docx")){
      document.location.href = "parser_docx/main.php?file=" + this.id;
    } else if(this.classList.contains("odt")) {
      document.location.href = "parser_odt/main.php?file=" + this.id;
    }
  }
}

for(i=0; i<nb_fichiers; i++){
  fichiers[i].onclick = function(){
    var exist_div = document.querySelector('.suppr_div');
    if(typeof(exist_div) != 'undefined' && exist_div != null){
      document.body.removeChild(document.querySelector('.suppr_div'));
    }
    var suppr_div = document.createElement('div');
    suppr_div.setAttribute('class', 'suppr_div');
    var suppr_button = document.createElement('button');
    suppr_button.setAttribute('class', 'suppr_button');
    suppr_button.setAttribute('id', 'suppr_' + this.id);
    suppr_button.textContent = 'Supprimer';
    var annule_button = document.createElement('button');
    annule_button.setAttribute('class', 'annule_button');
    annule_button.setAttribute('id', 'annule_' + this.id);
    annule_button.textContent = 'Annuler';
    suppr_div.appendChild(suppr_button);
    suppr_div.appendChild(annule_button);
    document.body.appendChild(suppr_div);
    suppr_button.onclick = function(){
      var nom_fichier = this.id.split('suppr_')[1];
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
          if (xhr.status === 200) {
            if(xhr.responseText == 'OK'){
              location.reload();
            }
          }
        }
      }
      xhr.open("POST", "suppr.php/?fichier=" + encodeURI(nom_fichier), true);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      xhr.send();
    }
    annule_button.onclick = function(){
      document.body.removeChild(document.querySelector('.suppr_div'));
    }
  }
}

var colorInit = document.querySelector('#dossier').style.backgroundColor;
var opacityInit = document.querySelector('#dossier').style.opacity;
var dropContainer = document.querySelector('#dossier');
dropContainer.ondragover = dropContainer.ondragenter = function(evt) {
  evt.preventDefault();
  this.style.backgroundColor = '#000';
  this.style.opacity = 0.5;
};

dropContainer.ondrop = function(evt) {
  fileInput.files = evt.dataTransfer.files;
  evt.preventDefault();

  var fichiers = document.querySelector('#fileInput'); 

  var fichier = fichiers.files[0];


  var data = new FormData();

  var request = new XMLHttpRequest();

  // File selected by the user
  // In case of multiple files append each of them
  data.append('fichier', fichier);

  // AJAX request finished
  request.addEventListener('load', function(e) {
  // request.response will hold the response from the server
        if(request.status == 200){
          document.querySelector('#dossier').style.backgroundColor = colorInit;
          document.querySelector('#dossier').style.opacity = opacityInit;
          if(request.responseText == 'OK'){           
            document.location.reload(true);
          } else {
            console.log(request.responseText);
          }
        } else {
          console.log("Erreur lors de l'upload");
        }
  });

  // Upload progress on request.upload
  request.upload.addEventListener('progress', function(e) {
  var percent_complete = (e.loaded / e.total)*100;
 
  // Percentage of upload completed
  //console.log(percent_complete);
  });

  // If server is sending a JSON response then set JSON response type
  request.responseType = 'text';

  // Send POST request to the server side script
  request.open('post', 'upload.php');
  request.send(data); 
};

