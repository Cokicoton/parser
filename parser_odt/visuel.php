<?php
function creation_visuel($corps, $titre) {

file_put_contents('visuel.html', "");

$fichier = fopen('visuel.html', 'a+');

$init = '<DOCTYPE! HTML>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" type="text/css" href="css/visuel.css">
</head>
<body>';

$end = '</body>
</html>';

fwrite($fichier, $init);
fwrite($fichier, '<div id="bloc">');
fwrite($fichier, '<h1>' . $titre . '</h1>');
fwrite($fichier, str_replace("\n", '<br>', str_replace('src="/images/', 'src="images/', $corps)));
fwrite($fichier, '</div>');
fwrite($fichier, '<script src="js/visuel.js"></script>');
fwrite($fichier, $end);
fclose($fichier);
return true;
}
?>
