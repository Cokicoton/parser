var champs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var more = 0;

var champs_ajoutes = [];
champs_ajoutes[0] = '<p style="margin: 0;text-align: right;"><a href="http://groupemarxiste.info/documents/NOMPDF">Lire le tract au format PDF</a></p>';
champs_ajoutes[1] = '<p style="margin: 0;text-align: justify;"><em><a href="REMPLACER">(Anglais / English)</a></em></p>';
champs_ajoutes[2] = '<p style="margin: 0;text-align: justify;"><a href="REMPLACER"><em>(Allemand / Deutsch)</em></a></p>';
champs_ajoutes[3] = '<p style="margin: 0;text-align: justify;"><a href="REMPLACER"><em>(Catalan / Catal&agrave;)</em></a></p>';
champs_ajoutes[4] = '<p style="margin: 0;text-align: justify;"><a href="REMPLACER"><em>(Espagnol / Española)</em></a></p>';
champs_ajoutes[5] = '<p style="margin: 0;text-align: justify;"><a href="REMPLACER"><em>(Portugais / Português)</em></a></p>';
champs_ajoutes[6] = '<p style="margin: 0;text-align: justify;"><a href="REMPLACER"><em>(Turc / Türk)</em></a></p>';
champs_ajoutes[7] = '<p style="margin: 0;text-align: justify;"><a href="REMPLACER"><em>(???????? / ????????)</em></a></p>';
champs_ajoutes[8] = '<p style="margin: 0;text-align: justify;"><a href="REMPLACER"><em>(Espéranto / Esperanto)</em></a></p>';
champs_ajoutes[9] = '<p style="margin: 0;text-align: justify;"><a href="REMPLACER"><em>(Italien / Italiano)</em></a></p>';
champs_ajoutes[10] = '<p style="margin: 0;text-align: justify;"><a href="REMPLACER"><em>(Russe / Русский)</em></a></p>';

var corps = document.getElementById('texte').textContent;

function maj_corps() {
  var temp = '';
  var i;
  for (i=0; i<=10; i++) {
    if (champs[i] == 1) {
      temp = temp + champs_ajoutes[i] + "\n";
    }
  }
  if (champs[0] + champs[1] + champs[2] + champs[3] + champs[4] + champs[5] + champs[6] + champs[7] + champs[8] + champs[9] + champs[10] > 0) { //Mise à jour du corps uniquement si une balise a été ajoutée
    document.getElementById('texte').textContent = temp + "\n\n" + corps;
  } else if (more == 1) {
    document.getElementById('texte').textContent = corps;
  }
}


var more = 0;
function ajouter_version($langue){
  var text;
  var valide = false;
  switch($langue) {
    case "anglais":
        if (champs[1] == 0) {
          champs[1] = 1;
          valide = true;
          document.cookie = "anglais=1";
        }
        break;
    case "allemand":
        if (champs[2] == 0) {
          champs[2] = 1;
          valide = true;
          document.cookie = "allemand=1";
        }
        break;
    case "catalan":
        if (champs[3] == 0) {
          champs[3] = 1;
          valide = true;
          document.cookie = "catalan=1";
        }
        break;
    case "espagnol":
        if (champs[4] == 0) {
          champs[4] = 1;
          valide = true;
          document.cookie = "espagnol=1";
        }
        break;
    case "portugais":
        if (champs[5] == 0) {
          champs[5] = 1;
          valide = true;
          document.cookie = "portugais=1";
        }
        break;
    case "turk":
        if (champs[6] == 0) {
          champs[6] = 1;
          valide = true;
          document.cookie = "turk=1";
        }
        break;
    case "esperanto":
        if (champs[8] == 0) {
          champs[8] = 1;
          valide = true;
          document.cookie = "esperanto=1";
        }
        break;
    case "italien":
        if (champs[9] == 0) {
          champs[9] = 1;
          valide = true;
          document.cookie = "italien=1";
        }
        break;
    case "russe":
        if (champs[10] == 0) {
          champs[10] = 1;
          valide = true;
          document.cookie = "russe=1";
        }
        break;
    case "divers":
        if (champs[7] == 0) {
          champs[7] = 1;
          valide = true;
          document.cookie = "divers=1";
        }
        break;
   }
   maj_corps();
}

function ajouter_pdf(){
  if (champs[0] == 0) {
    champs[0] = 1;
    maj_corps();
    document.cookie = "pdf=1";
  }
}

function retour_accueil() {
  var valide = window.confirm("Attention, vous êtes sur le point de retourner à l'accueil, les dossiers temp et images seront vidés. voulez-vous continuer ?");
  if (valide) {
    document.cookie = 'anglais=0';
    document.cookie = 'allemand=0';
    document.cookie = 'espagnol=0';
    document.cookie = 'portugais=0';
    document.cookie = 'turk=0';
    document.cookie = 'esperanto=0';
    document.cookie = 'italien=0';
    document.cookie = 'russe=0';
    document.cookie = 'divers=0';
    document.cookie = 'pdf=0';
    document.cookie = 'more=0';
    document.location.href = "/parser";
  }
}

function ajouter_more(){
  if (more == 0) {
    var string = corps;
    var temp, i;
    temp = 0;
    var taille_chaine = string.length;
    var fin = Math.min(taille_chaine - 1, 700)
    for (i=0; i< fin; i++) {
     if (((string[i] == ".") || (string[i] == "?") || (string[i] == "!")) && ((string[i+1] == " ") || (string[i+1] == "<"))) {
       temp = i;
     }
    }
    if (temp != 0) { // un ".", "?" ou "!" a été trouvé
      corps = string.slice(0, temp+1) + "<!--more-->" + string.slice(temp + 1, taille_chaine-1);
      more = 1;
      maj_corps();
      document.cookie = 'more=' + temp;
    } else { // aucun ".", "?" ou "!" n'a été trouvé
       for (i=0; i< fin; i++) {
         if ( (string[i] == ",") || (string[i] == ";") || (string[i] == " ") ) { //on cherche le dernier ",", ";", ou " "
           temp = i;
         }
       }
       if (temp != 0) { //un ",", ";", ou " " a été trouvé
        corps = string.slice(0, temp+1) + "<!--more-->" + string.slice(temp + 1, taille_chaine-1);
        more = 1;
        maj_corps();
        document.cookie = 'more=' + temp;
       } else { //aucun ",", ";", ou " " n'a été trouvé
         resultat = string;
         alert('Emplacement de la balise <!--more--> non trouvé'); // on informe l'utilisateur
       }
    }
  }
}

document.getElementById('injection').onclick = function() {
  ////////////////Corps////////////////////
  var texte = document.getElementById('texte').value;
  var texte_temp = texte.replace("\n", '\\n');
  while(texte_temp.indexOf("\n") != -1){
    texte_temp = texte_temp.replace("\n", '\\n');
  }
  texte_temp = texte_temp.replace(/"/g, '\\"');

  texte_temp = 'document.getElementById("content").value = "' + texte_temp + '";';

  ///////////////Titre////////////////////
  var titre = document.querySelector('#titre').value;
  while(titre.indexOf("\n") != -1){
    titre = titre.replace("\n", '\\n');
  }
  titre = titre.replace(/"/g, '\\"');

  texte_temp = texte_temp + "\n" + 'document.querySelector("#title-prompt-text").click();';
  texte_temp = texte_temp + "\n" + 'document.getElementById("title").value = "' + titre + '";';

  //////////////Date//////////////////////
  var date = document.querySelector('#date').textContent;
  date_temp = date.split('/');
  texte_temp = texte_temp + "\n" + 'document.getElementById("jj").value = "' + date_temp[0] + '";';
  texte_temp = texte_temp + "\n" + 'document.getElementById("mm").value = "' + date_temp[1] + '";';
  texte_temp = texte_temp + "\n" + 'document.getElementById("aa").value = "' + date_temp[2] + '";';
  texte_temp = texte_temp + "\n" + 'document.getElementById("hh").value = "12";';
  texte_temp = texte_temp + "\n" + 'document.getElementById("mn").value = "00";';


  /////////////Copie/////////////////////
  document.getElementById('texte').value = texte_temp;
  document.getElementById('texte').select();
  document.execCommand( 'copy' );
  document.getElementById('texte').value = texte;
};

document.getElementById('guttenberg').onclick = function() {
  ////////////////Corps////////////////////
  var texte = document.getElementById('texte').value;
  var texte_temp = texte.replace("\n", '\\n');
  while(texte_temp.indexOf("\n") != -1){
    texte_temp = texte_temp.replace("\n", '\\n');
  }
  texte_temp = texte_temp.replace(/"/g, '\\"');
  texte_temp = "wp.data.dispatch( 'core/editor' ).editPost( { content: \"" + texte_temp + "\" } );";

  ///////////////Titre////////////////////
  var titre = document.querySelector('#titre').value;
  while(titre.indexOf("\n") != -1){
    titre = titre.replace("\n", '\\n');
  }
  titre = titre.replace(/"/g, '\\"');
  texte_temp = texte_temp + "\n" + "wp.data.dispatch( 'core/editor' ).editPost( { title: \"" + titre +"\" } );";

  //////////////Date//////////////////////
  var date = document.querySelector('#date').textContent;
  date_temp = date.split('/');
  texte_temp = texte_temp + "\n" + "wp.data.dispatch( 'core/editor' ).editPost( { date: \"" + date_temp[2] + "-" + date_temp[1] + "-" + date_temp[0] + "T12:00:00\" } );";

  /////////////Copie/////////////////////
  document.getElementById('texte').value = texte_temp;
  document.getElementById('texte').select();
  document.execCommand( 'copy' );
  document.getElementById('texte').value = texte;
}

function envoi_ftp() {
  var url = document.location.href + "&ftp=oui";
  var request = new XMLHttpRequest();
  request.open('GET', url);
  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  request.send();
}

document.getElementById('ftp').onclick = function() {
  //alert('En pause en attendant résolution des images multiples');
  //envoi_ftp();
}

document.getElementById('copy').onclick = function() {
  document.getElementById('texte').select();
  document.execCommand( 'copy' );
};


document.getElementById('key_words').onclick = function() {
  var div = document.createElement('div');
  div.setAttribute('id', 'key_word_nodal');
  var input = document.createElement('input');
  input.setAttribute('id', 'key_word_saisie');
  input.setAttribute('placeholder', 'Mots-clés à découper');
  input.setAttribute('type', 'text');
  var bouton = document.createElement('input');
  bouton.setAttribute('value', 'Decouper');
  bouton.setAttribute('onclick', 'decoupe_key_words();');
  bouton.setAttribute('id', 'bouton_decoupe');
  bouton.setAttribute('type', 'button');
  var fermer = document.createElement('input');
  fermer.setAttribute('value', 'Fermer');
  fermer.setAttribute('onclick', 'fermer_key_words();');
  fermer.setAttribute('id', 'bouton_fermer');
  fermer.setAttribute('type', 'button');
  var boutons_nodal = document.createElement('div');
  boutons_nodal.setAttribute('id', 'boutons_nodal');
  div.appendChild(input);
  boutons_nodal.appendChild(bouton);
  boutons_nodal.appendChild(fermer);
  div.appendChild(boutons_nodal);
  document.querySelector('body').appendChild(div);
}

function decoupe_key_words() {
  var saisie = document.getElementById('key_word_saisie');
  var temp = saisie.value;
  while (temp.indexOf(', ') != -1) {
    temp = temp.replace(', ', ',');
  }
  saisie.value = temp;
  saisie.select();
  document.execCommand( 'copy' );
}

function fermer_key_words() {
  var nodal = document.getElementById('key_word_nodal');
  document.querySelector('body').removeChild(nodal);
}
