function read_cookie() {
  var corps = document.getElementById('bloc').textContent;
  var cookies = document.cookie;
  var tab_cookie = cookies.split('; ');
  var nb_cookie = tab_cookie.length;
  var i, temp, p, em, a, premier_enfant;
  for (i=0; i<nb_cookie; i++) {
    temp = tab_cookie[i].split('=');
    switch (temp[0]) {
      case 'anglais': if (temp[1] == "1") {
                        p = document.createElement("p");
                        p.setAttribute("style", "margin: 0;text-align: justify;");
                        em = document.createElement("em");
                        a = document.createElement("a");
                        a.setAttribute("href", "REMPLACER");
                        a.textContent = "(Anglais / English)";
                        em.appendChild(a);
                        p.appendChild(em);
                        div = document.querySelector('#bloc h1');
                        document.getElementById('bloc').insertBefore(p, div);
                      }
                      break;
      case 'allemand': if (temp[1] == "1") {
                         p = document.createElement("p");
                         p.setAttribute("style", "margin: 0;text-align: justify;");
                         em = document.createElement("em");
                         a = document.createElement("a");
                         a.setAttribute("href", "REMPLACER");
                         a.textContent = "(Allemand / Deutsch)";
                         em.appendChild(a);
                         p.appendChild(em);
                         div = document.querySelector('#bloc h1');
                         document.getElementById('bloc').insertBefore(p, div);
                       }
                       break;
      case 'catalan': if (temp[1] == "1") {
                           p = document.createElement("p");
                           p.setAttribute("style", "margin: 0;text-align: justify;");
                           em = document.createElement("em");
                           a = document.createElement("a");
                           a.setAttribute("href", "REMPLACER");
                           a.textContent = "Catalan / Català)";
                           em.appendChild(a);
                           p.appendChild(em);
                           div = document.querySelector('#bloc h1');
                           document.getElementById('bloc').insertBefore(p, div);
                         }
                         break;
      case 'espagnol': if (temp[1] == "1") {
                         p = document.createElement("p");
                         p.setAttribute("style", "margin: 0;text-align: justify;");
                         em = document.createElement("em");
                         a = document.createElement("a");
                         a.setAttribute("href", "REMPLACER");
                         a.textContent = "(Espagnol / Español)";
                         em.appendChild(a);
                         p.appendChild(em);
                         div = document.querySelector('#bloc h1');
                         document.getElementById('bloc').insertBefore(p, div);
                       }
                       break;
      case 'portugais': if (temp[1] == "1") {
                          p = document.createElement("p");
                          p.setAttribute("style", "margin: 0;text-align: justify;");
                          em = document.createElement("em");
                          a = document.createElement("a");
                          a.setAttribute("href", "REMPLACER");
                          a.textContent = "(Portugais / Português)";
                          em.appendChild(a);
                          p.appendChild(em);
                          div = document.querySelector('#bloc h1');
                          document.getElementById('bloc').insertBefore(p, div);
                        }
                        break;
      case 'turk': if (temp[1] == "1") {
                     p = document.createElement("p");
                     p.setAttribute("style", "margin: 0;text-align: justify;");
                     em = document.createElement("em");
                     a = document.createElement("a");
                     a.setAttribute("href", "REMPLACER");
                     a.textContent = "(Turc / Türk)";
                     em.appendChild(a);
                     p.appendChild(em);
                     div = document.querySelector('#bloc h1');
                     document.getElementById('bloc').insertBefore(p, div);
                   }
                   break;
      case 'esperanto': if (temp[1] == "1") {
                     p = document.createElement("p");
                     p.setAttribute("style", "margin: 0;text-align: justify;");
                     em = document.createElement("em");
                     a = document.createElement("a");
                     a.setAttribute("href", "REMPLACER");
                     a.textContent = "(Espéranto / Esperanto)";
                     em.appendChild(a);
                     p.appendChild(em);
                     div = document.querySelector('#bloc h1');
                     document.getElementById('bloc').insertBefore(p, div);
                   }
                   break;
      case 'italien': if (temp[1] == "1") {
                     p = document.createElement("p");
                     p.setAttribute("style", "margin: 0;text-align: justify;");
                     em = document.createElement("em");
                     a = document.createElement("a");
                     a.setAttribute("href", "REMPLACER");
                     a.textContent = "(Italien / Italiano)";
                     em.appendChild(a);
                     p.appendChild(em);
                     div = document.querySelector('#bloc h1');
                     document.getElementById('bloc').insertBefore(p, div);
                   }
                   break;
      case 'russe': if (temp[1] == "1") {
                     p = document.createElement("p");
                     p.setAttribute("style", "margin: 0;text-align: justify;");
                     em = document.createElement("em");
                     a = document.createElement("a");
                     a.setAttribute("href", "REMPLACER");
                     a.textContent = "(Russe / Русский)";
                     em.appendChild(a);
                     p.appendChild(em);
                     div = document.querySelector('#bloc h1');
                     document.getElementById('bloc').insertBefore(p, div);
                   }
                   break;
      case 'divers': if (temp[1] == "1") {
                       p = document.createElement("p");
                       p.setAttribute("style", "margin: 0;text-align: justify;");
                       em = document.createElement("em");
                       a = document.createElement("a");
                       a.setAttribute("href", "REMPLACER");
                       a.textContent = "(???????? / ????????)";
                       em.appendChild(a);
                       p.appendChild(em);
                       div = document.querySelector('#bloc h1');
                       document.getElementById('bloc').insertBefore(p, div);
                     }
                     break;
      case 'pdf': if (temp[1] == "1") {
                    p = document.createElement("p");
                    p.setAttribute("style", "text-align: right;");
                    em = document.createElement("em");
                    a = document.createElement("a");
                    a.setAttribute("href", "http://groupemarxiste.info/documents/NOMPDF");
                    a.textContent = "Lire le tract au format PDF";
                    em.appendChild(a);
                    p.appendChild(em);
                    premier_enfant = document.querySelector('#bloc').firstChild;
                    document.getElementById('bloc').insertBefore(p, premier_enfant);
                  }
                  break;
      case 'more': if (temp[1] != "0") {
                     /*var debut = corps.slice(0, parseInt(temp[1]));
                     var fin =  corps.slice(parseInt(temp[1])+1, corps.length);
                     document.getElementById('bloc').textContent = debut + '<!--more-->' + fin;*/
                   }
                   break;
    }
  }
}

read_cookie();
