var click = false; //l'utilisateur n'a pas cliqué dans le texteArea
var change = false;
var infos = false;
var debutChange = 0;
var finChange = 0;
var typeChange = '';
var selectionChange = ''; //sauvegarde de la dernière selection avant changement (pour le ctrl+z)
var positionBefore = 0; //sauvegarde de la dernière position avant changement (pour le ctrl+z)
var startBefore = -1; //sauvegarde de la dernière position avant changement (pour le ctrl+z) -1 si pas de selection
var tableShortCuts = ['alt+m', 'alt+i', 'alt+b', 'alt+h', 'alt+q', 'alt+u', 'alt+c', 'alt+l', 'alt+r', 'alt+p', 'ctrl+i', 'ctrl+z'];
var tableDescriptions = ['Insère la balise <!--more-->', 'Met la ligne ou la selection en italique', 'Met la ligne ou la selection en gras',
                         'Met la selection en sous-titre', 'Met la ligne ou la selection en citation', 'Met chaque ligne de la selection en liste',
                         'Met la ligne ou la sélection en centré', 'Met la sélection en lien',
                         'Supprime les balises de la ligne ou de la sélection', 'Supprime les puces de liste de la ligne/sélection',
                         'Affiche/Masque les raccourcis clavier', 'Annule la dernière modification par raccourci'];


function search_debut_de_ligne(texte, position){ //renvoi la position du premier caractère après saut de ligne avant de la position donnée
  for(var i=position; i>=0; i--){
    if(texte[i] == "\n" && i != position){
      return i + 1;
    }
  }
  return i+1;
}

function search_fin_de_ligne(texte, position){ //renvoi la position du dernier caractère avant saut de ligne à partir de la position donnée
  for(var i=position; i<texte.length; i++){
    if(texte[i] == "\n"){
      return i;
    }
  }
  return i-1;
}

document.querySelector('#texte').onclick = function(){
  click = true;
}


document.onkeydown = function(e){
   e = e || window.event;
   var cas = 0;

//////// Gestion des evenements lorsque un texte est selectionné
  if((document.querySelector('#texte').selectionStart < document.querySelector('#texte').selectionEnd) && cas == 0){
    cas = 1;
    if ((e.which == 73 || e.keyCode == 73) && e.ctrlKey) { //Afficher les infos sur les shortcuts ctrl+i
       if(infos){
         document.getElementById('infos').parentElement.removeChild(document.getElementById('infos'));
         infos = false;
       } else {
         var div = document.createElement('div');
         div.id = 'infos';
         div.style.backgroundColor = '#EEE';
         div.style.width = '560px';
         div.style.minHeight = '200px';
         div.style.position = 'fixed';
         div.style.top = '20%';
         div.style.left = '0px';
         div.style.right = '0px';
         div.style.margin = 'auto';
         div.style.padding = '12px';
         div.style.display = 'flex';
         div.style.border = '1px solid';
         div.style.flexDirection= "row";
         div.style.justifyContent = 'space-around';
         div.style.flexFlow= 'row wrap';
         for(var i = 0; i<tableShortCuts.length; i++){
           var item = document.createElement('div');
           item.setAttribute('class','raccourci');
           item.style.display = 'flex';
           item.style.flexDirection= 'row';
           item.style.padding = '5px';
           item.style.marginBottom = '5px';
           item.style.border = '1px dashed';
           item.style.borderRadius = '10px';
           item.style.width = '45%';
           var short = document.createElement('div');
           short.setAttribute('class', 'shortcut');
           short.style.width = '50px';
           short.style.fontWeight = 'bold';
           short.style.marginRight = '20px';
           short.textContent = tableShortCuts[i]; 
           var desc = document.createElement('div');
           desc.setAttribute('class','desc');
           desc.textContent = tableDescriptions[i];
           item.appendChild(short);
           item.appendChild(desc);
           div.appendChild(item);
         }
         document.body.appendChild(div);
         infos = true;
      }
    }
    if ((e.which == 77 || e.keyCode == 77) && e.altKey) { //insertion de la balise more alt+m
       var position = document.querySelector('#texte').selectionStart;
       var texte = document.querySelector('#texte').value;
       var debutTexte = document.querySelector('#texte').value.substr(0, position);
       var finTexte = document.querySelector('#texte').value.substr(position);
       document.querySelector('#texte').value = debutTexte + '<!--more-->' + finTexte;
       change = true;
       debutChange= position;
       finChange= position + 11; // inutile ici
       typeChange = 'more';
       positionBefore = position;
       startBefore = -1;
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 73 || e.keyCode == 73) && e.altKey) { //gestion de l'italique alt+i
       var debut = document.querySelector('#texte').selectionStart;
       var fin = document.querySelector('#texte').selectionEnd;
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       var replaceSelection = '<em>' + selection + '</em>';
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange= debut;
       finChange= fin + 9; // car <em></em> en plus
       typeChange = 'em';
       positionBefore = fin;
       startBefore = debut;
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 66 || e.keyCode == 66) && e.altKey) { //gestion du gras alt+b
       var debut = document.querySelector('#texte').selectionStart;
       var fin = document.querySelector('#texte').selectionEnd;
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       var replaceSelection = '<b>' + selection + '</b>';
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange= debut;
       finChange= fin + 7; // car <b></b> en plus
       typeChange = 'b';
       positionBefore = fin;
       startBefore = debut;
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 72 || e.keyCode == 72) && e.altKey) { //gestion des titres 3 alt+h
       var debut = document.querySelector('#texte').selectionStart;
       var fin = document.querySelector('#texte').selectionEnd;
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       var replaceSelection = '<h3>' + selection + '</h3>';
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange= debut;
       finChange= fin + 9; // car <h3></h3> en plus
       typeChange = 'h3';
       positionBefore = fin;
       startBefore = debut;
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 81 || e.keyCode == 81) && e.altKey) { //gestion des quotes alt+q
       var debut = document.querySelector('#texte').selectionStart;
       var fin = document.querySelector('#texte').selectionEnd;
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       var replaceSelection = "<quote class='citations'>" + selection + '</quote>';
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange= debut;
       finChange= fin + 33; // car <b></b> en plus
       typeChange = 'quote';
       positionBefore = fin;
       startBefore = debut;
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 85 || e.keyCode == 85) && e.altKey) { //gestion des listes alt+u
       var debut = document.querySelector('#texte').selectionStart;
       var fin = document.querySelector('#texte').selectionEnd;
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       selectionChange = selection;
       while(selection.match(/\n\n/g) !== null){
         selection = selection.replace(/\n\n/g, "\n")
       }
       var replaceSelection = '<ul><li>' + selection.replace(/\n/g, "</li>\n<li>") + "</li></ul>";
       replaceSelection = replaceSelection.replace(/<li><\/li>/g, '');
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange = debut;
       finChange = debutChange + replaceSelection.length;
       typeChange = 'ul';
       positionBefore = fin;
       startBefore = debut;
       document.querySelector('#texte').setSelectionRange(debutChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 67 || e.keyCode == 67) && e.altKey) { //gestion des center alt+c
       var debut = document.querySelector('#texte').selectionStart;
       var fin = document.querySelector('#texte').selectionEnd;
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       var replaceSelection = '<center>' + selection + '</center>';
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange= debut;
       finChange= fin + 17; // car <center></center> en plus
       typeChange = 'center';
       positionBefore = fin;
       startBefore = debut;
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 76 || e.keyCode == 76) && e.altKey) { //gestion des liens alt+l
       var debut = document.querySelector('#texte').selectionStart;
       var fin = document.querySelector('#texte').selectionEnd;
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       selectionChange = selection;
       var replaceSelection = '<a href="' + selection + '" target="_blank">' + selection + '</center>';
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange= debut;
       finChange= fin + 36 + selection.length; // car <a href="[selection]" target="_blank"></center> en plus
       typeChange = 'lien';
       positionBefore = fin;
       startBefore = debut;
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 82 || e.keyCode == 82) && e.altKey) { //suppression des balises alt+r
       var debut = document.querySelector('#texte').selectionStart;
       var fin = document.querySelector('#texte').selectionEnd;
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       selectionChange = selection;
       var replaceSelection = selection.replace(/(<([^>]+)>)/ig, '');
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange = debut;
       finChange = debutChange + replaceSelection.length;
       typeChange = 'r';
       positionBefore = fin;
       startBefore = debut;
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 80 || e.keyCode == 80) && e.altKey) { //suppression des puces de liste alt+p
       var debut = document.querySelector('#texte').selectionStart;
       var fin = document.querySelector('#texte').selectionEnd;
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       selectionChange = selection;
       var replaceSelection = selection.replace(/•(\\t)*/g, '').replace(/o(\\t)*/g, '').replace(/\n\-(\\t)*/g, "\n");
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange = debut;
       finChange = debutChange + replaceSelection.length;
       typeChange = 'r';
       positionBefore = fin;
       startBefore = debut;
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 90 || e.keyCode == 90) && e.ctrlKey) { //gestion de l'annulation ctrl+z
      if(change){ //seulement si des modifications ont été faites
        if(typeChange == 'ul' || typeChange == 'r' || typeChange == 'lien'){
          var debutTexte = document.querySelector('#texte').value.substr(0, debutChange);
          var finTexte = document.querySelector('#texte').value.substr(finChange);
          document.querySelector('#texte').value = debutTexte + selectionChange + finTexte;
          change = false;
          document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
        } else if(typeChange == 'quote') {
          var selection = document.querySelector('#texte').value.substr(debutChange, finChange-debutChange);
          var replaceTexte = document.querySelector('#texte').value.substr(0, debutChange);
          replaceTexte = replaceTexte + selection.substr(25, finChange - debutChange - 33);
          replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(finChange);
          document.querySelector('#texte').value = replaceTexte;
          change = false;
          document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
        } else if(typeChange == 'more'){
          document.querySelector('#texte').value = document.querySelector('#texte').value.replace(/<!--more-->/, '');
          change = false;
          document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
        } else {
          var selection = document.querySelector('#texte').value.substr(debutChange, finChange-debutChange);
          var replaceTexte = document.querySelector('#texte').value.substr(0, debutChange);
          replaceTexte = replaceTexte + selection.substr(2 + typeChange.length, finChange - debutChange - 2*typeChange.length -5);
          replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(finChange);
          document.querySelector('#texte').value = replaceTexte;
          change = false;
          document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
        }
        if(startBefore != -1){
          document.querySelector('#texte').setSelectionRange(startBefore,positionBefore);
        } else {
          document.querySelector('#texte').setSelectionRange(positionBefore,positionBefore);
        }
      }
    }
  }

//////// Gestion des evenements sans qu'un texte ne soit selectionné mais avec le curseur positionné
  if((document.querySelector('#texte').selectionStart == document.querySelector('#texte').selectionEnd) && click && cas == 0){
    cas = 2;
    var tailleLigne = search_fin_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionEnd) -search_debut_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);

    if ((e.which == 73 || e.keyCode == 73) && e.ctrlKey) { //Afficher les infos sur les shortcuts ctrl+i
       if(infos){
         document.getElementById('infos').parentElement.removeChild(document.getElementById('infos'));
         infos = false;
       } else {
         var div = document.createElement('div');
         div.id = 'infos';
         div.style.backgroundColor = '#EEE';
         div.style.width = '560px';
         div.style.minHeight = '200px';
         div.style.position = 'fixed';
         div.style.top = '20%';
         div.style.left = '0px';
         div.style.right = '0px';
         div.style.margin = 'auto';
         div.style.padding = '12px';
         div.style.display = 'flex';
         div.style.border = '1px solid';
         div.style.flexDirection= "row";
         div.style.justifyContent = 'space-around';
         div.style.flexFlow= 'row wrap';
         for(var i = 0; i<tableShortCuts.length; i++){
           var item = document.createElement('div');
           item.setAttribute('class','raccourci');
           item.style.display = 'flex';
           item.style.flexDirection= 'row';
           item.style.padding = '5px';
           item.style.marginBottom = '5px';
           item.style.border = '1px dashed';
           item.style.borderRadius = '10px';
           item.style.width = '45%';
           var short = document.createElement('div');
           short.setAttribute('class', 'shortcut');
           short.style.width = '50px';
           short.style.fontWeight = 'bold';
           short.style.marginRight = '20px';
           short.textContent = tableShortCuts[i]; 
           var desc = document.createElement('div');
           desc.setAttribute('class','desc');
           desc.textContent = tableDescriptions[i];
           item.appendChild(short);
           item.appendChild(desc);
           div.appendChild(item);
         }
         document.body.appendChild(div);
         infos = true;
      }
    }
    if ((e.which == 77 || e.keyCode == 77) && e.altKey) { //insertion de la balise more alt+m
       var position = document.querySelector('#texte').selectionStart;
       var texte = document.querySelector('#texte').value;
       var debutTexte = document.querySelector('#texte').value.substr(0, position);
       var finTexte = document.querySelector('#texte').value.substr(position);
       document.querySelector('#texte').value = debutTexte + '<!--more-->' + finTexte;
       change = true;
       debutChange= position;
       finChange= position + 11; // inutile ici
       typeChange = 'more';
       positionBefore = position;
       startBefore = -1;
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 73 || e.keyCode == 73) && e.altKey && tailleLigne > 0) { //gestion de l'italique alt+i
       positionBefore = document.querySelector('#texte').selectionStart;
       startBefore = -1;
       var debut = search_debut_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var fin = search_fin_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       var replaceSelection = '<em>' + selection + '</em>';
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange= debut;
       finChange= fin + 9; // car <em></em> en plus
       typeChange = 'em';
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 66 || e.keyCode == 66) && e.altKey && tailleLigne > 0) { //gestion du gras alt+b
       positionBefore = document.querySelector('#texte').selectionStart;
       startBefore = -1;
       var debut = search_debut_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var fin = search_fin_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       var replaceSelection = '<b>' + selection + '</b>';
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange= debut;
       finChange= fin + 7; // car <b></b> en plus
       typeChange = 'b';
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 72 || e.keyCode == 72) && e.altKey && tailleLigne > 0) { //gestion des titres 3 alt+h
       positionBefore = document.querySelector('#texte').selectionStart;
       startBefore = -1;
       var debut = search_debut_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var fin = search_fin_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       var replaceSelection = '<h3>' + selection + '</h3>';
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange= debut;
       finChange= fin + 9; // car <h3></h3> en plus
       typeChange = 'h3';
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 81 || e.keyCode == 81) && e.altKey && tailleLigne > 0) { //gestion des quotes alt+q
       positionBefore = document.querySelector('#texte').selectionStart;
       startBefore = -1;
       var debut = search_debut_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var fin = search_fin_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       var replaceSelection = "<quote class='citations'>" + selection + '</quote>';
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange= debut;
       finChange= fin + 33; // car <b></b> en plus
       typeChange = 'quote';
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 67 || e.keyCode == 67) && e.altKey && tailleLigne > 0) { //gestion des center alt+c
       positionBefore = document.querySelector('#texte').selectionStart;
       startBefore = -1;
       var debut = search_debut_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var fin = search_fin_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       var replaceSelection = '<center>' + selection + '</center>';
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange= debut;
       finChange= fin + 17; // car <center></center> en plus
       typeChange = 'center';
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 82 || e.keyCode == 82) && e.altKey && tailleLigne > 0) { //suppression des balises de la ligne alt+r
       positionBefore = document.querySelector('#texte').selectionStart;
       startBefore = -1;
       var debut = search_debut_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var fin = search_fin_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       selectionChange = selection;
       var replaceSelection = selection.replace(/(<([^>]+)>)/ig, '');
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange = debut;
       finChange = debutChange + replaceSelection.length;
       typeChange = 'r';
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 80 || e.keyCode == 80) && e.altKey && tailleLigne > 0) { //suppression des puces de liste de la ligne alt+p
       positionBefore = document.querySelector('#texte').selectionStart;
       startBefore = -1;
       var debut = search_debut_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var fin = search_fin_de_ligne(document.querySelector('#texte').value, document.querySelector('#texte').selectionStart);
       var texte = document.querySelector('#texte').value;
       var selection = texte.substr(debut, fin-debut);
       selectionChange = selection;
       var replaceSelection = selection.replace(/•(\\t)*/g, '').replace(/o(\\t)*/g, '').replace(/\n\-(\\t)*/g, "\n");
       var replaceTexte = document.querySelector('#texte').value.substr(0, debut);
       replaceTexte = replaceTexte + replaceSelection;
       replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(fin);
       document.querySelector('#texte').value = replaceTexte;
       change = true;
       debutChange = debut;
       finChange = debutChange + replaceSelection.length;
       typeChange = 'r';
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 90 || e.keyCode == 90) && e.ctrlKey) { //gestion de l'annulation ctrl+z
      if(change){ //seulement si des modifications ont été faites
        if(typeChange == 'ul' || typeChange == 'r' || typeChange == 'lien'){
          var debutTexte = document.querySelector('#texte').value.substr(0, debutChange);
          var finTexte = document.querySelector('#texte').value.substr(finChange);
          document.querySelector('#texte').value = debutTexte + selectionChange + finTexte;
          change = false;
          document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
        } else if(typeChange == 'quote') {
          var selection = document.querySelector('#texte').value.substr(debutChange, finChange-debutChange);
          var replaceTexte = document.querySelector('#texte').value.substr(0, debutChange);
          replaceTexte = replaceTexte + selection.substr(25, finChange - debutChange - 33);
          replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(finChange);
          document.querySelector('#texte').value = replaceTexte;
          change = false;
          document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
        } else if(typeChange == 'more'){
          document.querySelector('#texte').value = document.querySelector('#texte').value.replace(/<!--more-->/, '');
          change = false; 
          document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
        } else {
          var selection = document.querySelector('#texte').value.substr(debutChange, finChange-debutChange);
          var replaceTexte = document.querySelector('#texte').value.substr(0, debutChange);
          replaceTexte = replaceTexte + selection.substr(2 + typeChange.length, finChange - debutChange - 2*typeChange.length -5);
          replaceTexte = replaceTexte + document.querySelector('#texte').value.substr(finChange);
          document.querySelector('#texte').value = replaceTexte;
          change = false;
          document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
        }        
        if(startBefore != -1){
          document.querySelector('#texte').setSelectionRange(startBefore,positionBefore);
        } else {
          document.querySelector('#texte').setSelectionRange(positionBefore,positionBefore);
        }
      }
    }
  }

//////// Gestion des evenements sans qu'un texte ne soit selectionné ni le curseur positionné
  if(!click && cas == 0){
    cas = 3;
    e = e || window.event;
    if ((e.which == 73 || e.keyCode == 73) && e.ctrlKey) { //Afficher les infos sur les shortcuts ctrl+i
       if(infos){
         document.getElementById('infos').parentElement.removeChild(document.getElementById('infos'));
         infos = false;
       } else {
         var div = document.createElement('div');
         div.id = 'infos';
         div.style.backgroundColor = '#EEE';
         div.style.width = '560px';
         div.style.minHeight = '200px';
         div.style.position = 'fixed';
         div.style.top = '20%';
         div.style.left = '0px';
         div.style.right = '0px';
         div.style.margin = 'auto';
         div.style.padding = '12px';
         div.style.display = 'flex';
         div.style.border = '1px solid';
         div.style.flexDirection= "row";
         div.style.justifyContent = 'space-around';
         div.style.flexFlow= 'row wrap';
         for(var i = 0; i<tableShortCuts.length; i++){
           var item = document.createElement('div');
           item.setAttribute('class','raccourci');
           item.style.display = 'flex';
           item.style.flexDirection= 'row';
           item.style.padding = '5px';
           item.style.marginBottom = '5px';
           item.style.border = '1px dashed';
           item.style.borderRadius = '10px';
           item.style.width = '45%';
           var short = document.createElement('div');
           short.setAttribute('class', 'shortcut');
           short.style.width = '50px';
           short.style.fontWeight = 'bold';
           short.style.marginRight = '20px';
           short.textContent = tableShortCuts[i]; 
           var desc = document.createElement('div');
           desc.setAttribute('class','desc');
           desc.textContent = tableDescriptions[i];
           item.appendChild(short);
           item.appendChild(desc);
           div.appendChild(item);
         }
         document.body.appendChild(div);
         infos = true;
      }
    }
    if ((e.which == 77 || e.keyCode == 77) && e.altKey && document.querySelector('#texte').selectionStart != 0) { //insertion de la balise more alt+m
       var position = document.querySelector('#texte').selectionStart;
       var texte = document.querySelector('#texte').value;
       var debutTexte = document.querySelector('#texte').value.substr(0, position);
       var finTexte = document.querySelector('#texte').value.substr(position);
       document.querySelector('#texte').value = debutTexte + '<!--more-->' + finTexte;
       change = true;
       debutChange= position;
       finChange= position + 11; // inutile ici
       typeChange = 'more';
       positionBefore = position;
       startBefore = -1;
       document.querySelector('#texte').setSelectionRange(finChange, finChange);
       document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
    }
    if ((e.which == 90 || e.keyCode == 90) && e.ctrlKey) { //gestion de l'annulation ctrl+z
      if(change){ //seulement si des modifications ont été faites
        if(typeChange == 'more'){
          document.querySelector('#texte').value = document.querySelector('#texte').value.replace(/<!--more-->/, '');
          change = false;
          document.cookie = "textContent=" + encodeURIComponent(document.querySelector('#texte').value);
        }
      }
    }
  }
}
