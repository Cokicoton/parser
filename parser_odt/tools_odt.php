<?php

function replace_crochet($text, $name, $family, $font_weight, $font_style, $align){
  $text_temp = $text;
  //Gestion des style spécifiques
  /*if(strpos($text_temp, '[T2]')){
    echo '------------------------T2--------------------------<br>';
    var_dump($name);
    var_dump($family);
    var_dump($font_weight);
    var_dump($font_style);
    var_dump($align);
    echo '----------------------T2-FIN----------------------<br>';
  }
  if(strpos($text_temp, '[T8]')){
    echo '------------------------T8--------------------------<br>';
    var_dump($name);
    var_dump($family);
    var_dump($font_weight);
    var_dump($font_style);
    var_dump($align);
    echo '----------------------T8-FIN----------------------<br>';
  }*/
  
  foreach($name as $key => $value){
    if($value == 'Titre' or $value == 'Titre1' or $value == 'Titre2'){
      $text_temp = str_replace('[' . $key . ']', '<h3>', $text_temp);
      $text_temp = str_replace('[/' . $key . ']', '</h3>', $text_temp);
    }
    if($value == 'Citation' || $value == 'CitationCar'){
      $text_temp = str_replace('[' . $key . ']', '<quote class="citations">', $text_temp);
      $text_temp = str_replace('[/' . $key . ']', '</quote>', $text_temp);
    }
  }
  foreach($family as $key => $value){
    if($value == '???' or $value == '???' or $value == '???'){
      $text_temp = str_replace('[' . $key . ']', '???', $text_temp);
      $text_temp = str_replace('[/' . $key . ']', '????', $text_temp);
    }
  }
  foreach($font_weight as $key => $value){
    if($value == 'bold'){
      $text_temp = str_replace('[' . $key . ']', '<b>', $text_temp);
      $text_temp = str_replace('[/' . $key . ']', '</b>', $text_temp);
    }
  }
  foreach($font_style as $key => $value){
    if($value == 'italic'){      
      $text_temp = str_replace('[' . $key . ']', '<em>', $text_temp);
      $text_temp = str_replace('[/' . $key . ']', '</em>', $text_temp);
    }
  }
  foreach($align as $key => $value){
    if($value == 'center'){
      $text_temp = str_replace('[' . $key . ']', '<center>', $text_temp);
      $text_temp = str_replace('[/' . $key . ']', '</center>', $text_temp);
    } elseif($value == 'right'){
      $text_temp = str_replace('[' . $key . ']', '<div style="text-align:right;">', $text_temp);
      $text_temp = str_replace('[/' . $key . ']', '</div>', $text_temp);
    } elseif($value == 'left'){
      $text_temp = str_replace('[' . $key . ']', '<div style="text-align:left;">', $text_temp);
      $text_temp = str_replace('[/' . $key . ']', '</div>', $text_temp);
    }
  }
  foreach($name as $key => $value){ //Il reste encore des crochets non gérés   
    $text_temp = str_replace('[' . $key . ']', '', $text_temp);
    $text_temp = str_replace('[/' . $key . ']', '', $text_temp);
  }

  //Gestion des style standards
  $text_temp = str_replace('[Titre]', '<h3>', $text_temp);
  $text_temp = str_replace('[/Titre]', '</h3>', $text_temp);
  $text_temp = str_replace('[Titre1]', '<h3>', $text_temp);
  $text_temp = str_replace('[/Titre1]', '</h3>', $text_temp);
  $text_temp = str_replace('[Titre2]', '<h3>', $text_temp);
  $text_temp = str_replace('[/Titre2]', '</h3>', $text_temp);
  $text_temp = str_replace('[Citation]', '<quote class="citations">', $text_temp);
  $text_temp = str_replace('[/Citation]', '</quote>', $text_temp);
  $text_temp = str_replace('[CitationCar]', '<quote class="citations">', $text_temp);
  $text_temp = str_replace('[/CitationCar]', '</quote>', $text_temp);
  $text_temp = str_replace('[Citationintense]', '<quote class="citations">', $text_temp);
  $text_temp = str_replace('[/Citationintense]', '</quote>', $text_temp);
  $text_temp = str_replace('[Emphasepâle]', '<em>', $text_temp);
  $text_temp = str_replace('[/Emphasepâle]', '</em>', $text_temp);
  $text_temp = str_replace('[Emphase]', '<em>', $text_temp);
  $text_temp = str_replace('[/Emphase]', '</em>', $text_temp);  
  return $text_temp;
}


function replace_span($balises){ //remplace les span par leur contenu
  $balises_temp = $balises;
  $resultat = '';
  while(count(explode('<text:span', $balises_temp)) > 1){    
    $debut = strpos($balises_temp, '<text:span');
    $fin = strpos($balises_temp, '</text:span>');
    $resultat = $resultat . explode('<text:span', $balises_temp)[0];    
    $span_complet = substr($balises_temp, $debut, $fin - $debut);   
    $style = explode('">', explode('text:style-name="', $span_complet)[1])[0];
    $span_contenu = explode('</text:span', explode('">', $span_complet)[1])[0];
    $balises_temp = substr($balises_temp, $fin + 12);
    $resultat = $resultat . '[' . $style . ']' . $span_contenu . '[/' . $style . ']';
  } 
  $resultat = $resultat . $balises_temp;

  return $resultat;
}

function recup_text($balises){
  $balises_temp = '%' . str_replace('<text:s/>', '', str_replace('<text:line-break/>', "\n", replace_span($balises))) . '%'; //Gestion du i - 1 et du i + 1
  $start = -1;
  $resultat = '';
  for($i=0; $i < strlen($balises_temp); $i++){    
    if($balises_temp[$i] == '>' && $balises_temp[$i + 1] != '<' && $balises_temp[$i + 1] != '%'){
      $start = $i + 1;
    }
    if($balises_temp[$i] == '<' && $balises_temp[$i - 1] != '>' && $start != -1) {
      $end = $i - 1; 
      $resultat = $resultat . substr($balises_temp, $start, $end - $start + 1);
    }
  }
  return $resultat;
}

function recup_styles($balises){
  $balises_temp = replace_span($balises);
  $styles = [];
  while(count(explode('text:style-name="', $balises_temp)) > 1){
    $style = explode('"',explode('text:style-name="', $balises_temp)[1])[0];
    array_push($styles, $style);
    $balises_temp = substr($balises_temp, strpos($balises_temp, 'text:style-name="') + 17);
  }

  return $styles;
}

function create_table($balises){  
  $temp = preg_replace('/<table:table-columns[:|\-| |=|"|\'|\/|a-z|A-Z|0-9]*>/', '', $balises);//On ne gère pas le style des colonnes donc inutiles
  $temp = preg_replace('/<\/table:table-columns>/', '', $temp);//On ne gère pas le style des colonnes donc inutiles
  $temp = preg_replace('/<table:table-column[:|\-| |=|"|\'|\/|a-z|A-Z|0-9]*>/', '', $temp);//On ne gère pas le style des colonnes donc inutiles
  $temp = preg_replace('/<table:table-row[:|\-| |=|"|\'|a-z|A-Z|0-9]*>/', '<tr>', $temp);
  $temp = preg_replace('/<\/table:table-row>/', '</tr>' . "\n", $temp);
  $temp = preg_replace('/<table:table-cell[:|\-| |=|"|\'|a-z|A-Z|0-9]*>/', '<td>', $temp);
  $temp = preg_replace('/<\/table:table-cell>/', '</td>' . "\n", $temp);
  $temp = preg_replace('/<table:table[:|\-| |=|"|\'|a-z|A-Z|0-9]*>/', '<table>', $temp);
  $temp = preg_replace('/<\/table:table>/', '</table>' . "\n", $temp);
  $temp = '<style>
table{
    border-collapse: collapse;
}
table, td{
    border:1px solid;
}
</style>' . "\n" . $temp;
  return $temp;
}

function genere_html($text, $style_standard, $style_name, $style_family, $style_font_weight, $style_font_style, $style_align){  
  $result = $text;
  $space = true; //Ajouter un espace à la fin
  $retour_chariot = true;

  //////////////////////////////Gestion du style standard//////////////////////////////  
  if(count($style_standard) > 0){
    //Gestion des titres
    if(in_array('Titre', $style_standard) || in_array('Titre1', $style_standard) || in_array('Titre2', $style_standard)){
      $result = "\n" . '<h3>' . $result . '</h3>' . "\n";
      $space = false;
      $retour_chariot = false;
    }
    //Gestion des citation
    if(in_array('Citation', $style_standard) || in_array('Citationintense', $style_standard) || in_array('CitationCar', $style_standard)){
      $result = "\n" . '<quote class="citations">' . $result . '</quote>' . "\n";
      $space = false;
      $retour_chariot = false;
    }
    //Gestion des italiques
    if(in_array('Emphasepâle', $style_standard) || in_array('Emphase', $style_standard)){ //Vérif si existe Emphase
      $result = '<em>' . $result . '</em>';
      $retour_chariot = false;
    }
  }

  

  ////////////////////////////Gestion du styles spécifiques////////////////////////////
  //Gestion du style général
  if(count($style_name) > 0) {
    if(in_array('Titre', $style_name) || in_array('Titre1', $style_name) || in_array('Titre2', $style_name)){
      $result = "\n" . '<h3>' . $result . '</h3>' . "\n";
      $space = false;
      $retour_chariot = false;
    }
    if(in_array('Citation', $style_name) || in_array('CitationCar', $style_name)){
      $result = "\n" . '<quote class="citations">' . $result . '</quote>' . "\n";
      $space = false;
      $retour_chariot = false;
    } // Les autres cas 'Normal' ne donne pas lieu à de nouvelles balises et 'Paragraphedeliste' est géré ail

  }

  //Gestion du style général bis
  if(count($style_family) > 0) {
    if(in_array('????', $style_family)){ //Différent de 'paragraph'
      $result = '???' . $result . '????';
    }
  }

  //Gestion du poids de la police
  if(count($style_font_weight) > 0) {
    if(in_array('bold', $style_font_weight)){
      $result = '<b>' . $result . '</b>';
      $retour_chariot = false;
    }
  }

  //Gestion du style de la police
  if(count($style_font_style) > 0) {
    if(in_array('italic', $style_font_style)){
      $result = '<em>' . $result . '</em>';
    }
  }

  //Gestion de l'alignement
  if(count($style_align) > 0) {
    if(in_array('center', $style_align)){
      $result = "\n" . '<center>' . $result . '</center>' . "\n";
      $space = false;
      $retour_chariot = false;
    } elseif(in_array('right', $style_align)){
      $result = "\n" . '<div style="text-align:right;">' . $result . '</div>' . "\n";
      $space = false;
      $retour_chariot = false;
    } elseif(in_array('left', $style_align)){
      $result = "\n" . '<div style="text-align:left;">' . $result . '</div>' . "\n";
      $space = false;
      $retour_chariot = false;
    } 
  }

  if($space){
    $result = $result . ' ';
  }
  if($retour_chariot){
    $result = $result . "\n\n";
  }
  return $result;
}

function recup_liste_text($balises){

  $temp = explode('<text:list-item>', $balises);
  $temp_item_clean = [];
  $resultats = [];

  for($i=1; $i<count($temp);$i++){
    array_push($temp_item_clean, explode('</text:list-item>', $temp[$i])[0]);
  }
  foreach($temp_item_clean as $item){
    array_push($resultats, recup_text($item));
  }
  return $resultats;
}

function recup_liste_styles($balises){
  $styles = [];
  $temp = explode('<text:list-item>', $balises);
  for($i=1; $i<count($temp);$i++){
    $style_temp = [];
    $balises_temp = $temp[$i];
    while(count(explode('text:style-name="', $balises_temp)) > 1){
      $style = explode('"',explode('text:style-name="', $balises_temp)[1])[0];
      array_push($style_temp, $style);
      $balises_temp = substr($balises_temp, strpos($balises_temp, 'text:style-name="') + 17);
    }
    array_push($styles, $style_temp);
  }
  return $styles;
}


function genere_liste_html($textes, $style_standard, $style_name, $style_family, $style_font_weight, $style_font_style, $style_align){
  $resultat =  '';


  for($i=0; $i<count($textes);$i++){
    $resultat_temp = $textes[$i];
    //Gestion du poids de la police
    if(count($style_font_weight[$i]) > 0) {
      if(in_array('bold', $style_font_weight[$i])){
        $resultat_temp = '<b>' . $resultat_temp . '</b>';
      }
    }
 
    //Gestion du style de la police
    if(count($style_font_style[$i]) > 0) {
      if(in_array('italic', $style_font_style[$i])){
        $resultat_temp = '<em>' . $resultat_temp . '</em>';
      }
    }
    $resultat_temp_sans_crochet = replace_crochet($resultat_temp, $style_name, $style_family, $style_font_weight, $style_font_style, $style_align);
    $resultat = $resultat . '<li>' . $resultat_temp_sans_crochet . '</li>' . "\n";
  }
  return "\n" . '<ul>' . "\n" . $resultat .'</ul>' . "\n";
}

function nettoie_balise_doublon($texte){
  $texte_temp = $texte;
  $liste_balises = ['b', 'em', 'center', 'h3', 'li'];
  foreach($liste_balises as $balises){
    while(strpos($texte_temp, '<' .$balises . '><' . $balises .'>') != false && strpos($texte_temp, '</' .$balises . '></' . $balises .'>') != false){
      $texte_temp = str_replace('<' .$balises . '><' . $balises .'>', '<' .$balises . '>', $texte_temp);
      $texte_temp = str_replace('</' .$balises . '></' . $balises .'>', '</' .$balises . '>', $texte_temp);
    }
  }
  return $texte_temp;
}
?>
