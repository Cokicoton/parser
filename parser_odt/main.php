<DOCTYPE! HTML>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <title>CMS Assistant</title>
</head>
<body>

<?php
include_once "parse_odt.php";
include_once "tools_odt.php";
include_once "visuel.php";



//$document = fopen('document/Titre.zip', 'r+');
if (!is_dir("temp")) {mkdir("temp");}
if (!is_dir("images")) {mkdir("images");}

$fichier = "document/" . $_GET["file"];

$regex = "/^[0-9]{4} [0-9]{2} [0-9]{2}\w*/";
if (preg_match($regex, substr($fichier, 9))) {
  $temp = explode(" ", substr($fichier, 9));
  $year = $temp[0];
  $month = $temp[1];
  $day = $temp[2];
  $date = $day . "/" . $month . "/" . $year;
} else {
  $date = "unknown";
}

if (!file_exists("temp/META_INF")) {
  //$commande1 = "copy " . str_replace(' ', '\ ', $fichier) . " " . str_replace('.odt', '.zip', str_replace(' ', '\ ', $fichier)); //windows
  //exec($commande1, $resultat); //windows
  $commande = "unzip ../" . str_replace('.odt', '.odt', str_replace(' ', '\ ', $fichier)) . " -d temp"; //linux
  //$commande = 'c:/"Program Files"/7-Zip/7z.exe x c:/wamp64/www/parser_odt/' . str_replace('.odt', '.zip', str_replace(' ', '\ ', $fichier)) . " -oc:/wamp64/www/parser_odt/temp"; //windows
  exec($commande, $resultat);
} else {
  $console = "Document existant...<br>\n";
}

$file=fopen("temp/content.xml", "r");

while(!feof($file)){
  $ligne = fgets($file);
}

//Normalement la deuxième ligne contient le document entier sous forme xml
$start_style = strpos($ligne, '<office:automatic-styles>'); //début des styles
$stop_style = strpos($ligne, '</office:automatic-styles>'); //fin des styles
$start_body = strpos($ligne, '<office:body>');//début du body
$stop_body = strpos($ligne, '</office:body>');//fin du body

$style = substr($ligne, $start_style, $stop_style);
$body = substr($ligne, $start_body, $stop_body);
$body = str_replace('<text:s/>', ' ', $body);

$temp_style = $style;
while(count(explode('<style:style style:name="', $temp_style)) > 1){
$nom = explode('"', explode('<style:style style:name="', $temp_style)[1])[0];

$style_du_nom = explode('<style:style style:name="', $temp_style)[1];
if(strpos($style_du_nom, 'style:parent-style-name="') !== false){
  $style_name = explode('"', explode('style:parent-style-name="', $style_du_nom)[1])[0];
  $tab_style_name[$nom] = $style_name;
} else {
  $tab_style_name[$nom] = null;
}

if(strpos($style_du_nom, 'style:family="') !== false){
  $style_family = explode('"', explode('style:family="', $style_du_nom)[1])[0];
  $tab_style_family[$nom] = $style_family;
} else {
  $tab_style_family[$nom] = null;
}

if(strpos($style_du_nom, 'fo:font-weight="') !== false){
  $style_font_weight = explode('"', explode('fo:font-weight="', $style_du_nom)[1])[0];
  $tab_style_font_weight[$nom] = $style_font_weight;
} else {
  $tab_style_font_weight[$nom] = null;
}

if(strpos($style_du_nom, 'fo:font-style="') !== false){
  $style_font_style = explode('"', explode('fo:font-style="', $style_du_nom)[1])[0];
  $tab_style_font_style[$nom] = $style_font_style;
} else {
  $tab_style_font_style[$nom] = null;
}

if(strpos($style_du_nom, 'fo:text-align="') !== false){
  $style_align = explode('"', explode('fo:text-align="', $style_du_nom)[1])[0];
  $tab_style_align[$nom] = $style_align;
} else {
  $tab_style_align[$nom] = null;
}

$temp_style = explode('<style:style style:name="' . $nom . '"', $temp_style)[1];
}

$ouvert =0;
$paquets = [];
$j=strpos($body, '<office:text'); //Le document (texte) début après la balise <office:text...
while($body[$j] != '>'){
  $j++;
}
$start_body = $j;
$end_body = strpos($body, '</office');
$body_temp = explode('</office', substr($body, $start_body))[0] . '%'; //On ne prend pas les balises <office...>. Le % sert à gérer le $i +1 à la fin

for($i=0; $i<= strlen($body_temp) - 1; $i++){
  if ($body_temp[$i] == '<' && $body_temp[$i+1] != '/'){
    $ouvert++;
    if($ouvert == 1) {
      $start_pos = $i;
    }
  } elseif ($body_temp[$i] == '<' && $body_temp[$i+1] == '/'){
    $ouvert--;
    if($ouvert == 0) {
      $end_pos = $i + strpos(substr($body_temp, $i), '>');
      array_push($paquets, substr($body_temp, $start_pos, $end_pos - $start_pos + 1));

    }
  } elseif ($body_temp[$i] == '/' && $body_temp[$i+1] == '>'){ //balise autofermante
      $ouvert--;
    if($ouvert == 0) {

    }
  }
}

$fic = '';

foreach($paquets as $balises){
  if(preg_match('/^<text:[a|p|h]/', $balises)){ //Texte
    $text_temp = recup_text($balises);
    $text = replace_crochet($text_temp, $tab_style_name, $tab_style_family, $tab_style_font_weight, $tab_style_font_style, $tab_style_align);
    $styles = recup_styles($balises);
    $standard = [];
    $name = [];
    $family = [];
    $font_weight = [];
    $font_style = [];
    $align = [];
    foreach($styles as $style){
      if(array_key_exists($style, $tab_style_name)){
        array_push($name, $tab_style_name[$style]);
        array_push($family, $tab_style_family[$style]);
        array_push($font_weight, $tab_style_font_weight[$style]);
        array_push($font_style, $tab_style_font_style[$style]);
        array_push($align, $tab_style_align[$style]);
      } else {
        array_push($standard, $style);
      }
    }
    $fic = $fic . genere_html($text, $standard, $name, $family, $font_weight, $font_style, $align);

  } elseif(preg_match('/^<text:list/', $balises)) { //Liste

    $textes = recup_liste_text($balises);
    $styles = recup_liste_styles($balises);
    $standard = [];
    $name = [];
    $family = [];
    $font_weight = [];
    $font_style = [];
    $align = [];
    foreach($styles as $temp){
      $temp_name = [];
      $temp_family = [];
      $temp_font_weight = [];
      $temp_font_style = [];
      $temp_align = [];
      $temp_standard = [];
      foreach($temp as $style){
        if(array_key_exists($style, $tab_style_name)){
          array_push($temp_name, $tab_style_name[$style]);
          array_push($temp_family, $tab_style_family[$style]);
          array_push($temp_font_weight, $tab_style_font_weight[$style]);
          array_push($temp_font_style, $tab_style_font_style[$style]);
          array_push($temp_align, $tab_style_align[$style]);
        } else {
          array_push($temp_standard, $style);
        }
      }
      array_push($name, $temp_name);
      array_push($family, $temp_family);
      array_push($font_weight, $temp_font_weight);
      array_push($font_style, $temp_font_style);
      array_push($align, $temp_align);
      array_push($standard, $temp_standard);
    }
    $textes_temp = genere_liste_html($textes, $standard, $name, $family, $font_weight, $font_style, $align);
    $fic = $fic . replace_crochet($textes_temp, $tab_style_name, $tab_style_family, $tab_style_font_weight, $tab_style_font_style, $tab_style_align);

  } elseif(preg_match('/^<table:table/', $balises)) { //tableau
    $temp = create_table($balises);
    $squelette_table_temp = preg_replace('/<text[:|\-| |=|"|\'|\/|a-z|A-Z|0-9|\/>|é|è|ê|ë|î|ï|ù|û|ü|à|â|&|;|’|,|\.|\?|!]*\/>/', '_-_', $temp);
    $squelette_table_temp = preg_replace('/<text[:|\-| |=|"|\'|\/|a-z|A-Z|0-9|\/>|é|è|ê|ë|î|ï|ù|û|ü|à|â|&|;|’|,|\.|\?|!]*<\/text:[a|p|h]>/', '_-_', $squelette_table_temp);
    $squelette_table = preg_replace('/<td>[:|\-| |=|"|\'|\/|a-z|A-Z|0-9|\/>|é|è|ê|ë|î|ï|ù|û|ü|à|â|&|;|’|,|\.|\?|!]*<\/td>/', '<td>_-_</td>', $squelette_table_temp);

    $temp_td = explode('<td>', $temp);
    $cases = [];
    $first = 0;
    foreach($temp_td as $td){
      if($first == 0){
        $first++;
      } else {
        if(preg_match('/^<text:[a|p|h]/', $td)) { //Texte
          $text_temp = recup_text($td);
          $text = replace_crochet($text_temp, $tab_style_name, $tab_style_family, $tab_style_font_weight, $tab_style_font_style, $tab_style_align);
          $styles = recup_styles($td);
          $standard = [];
          $name = [];
          $family = [];
          $font_weight = [];
          $font_style = [];
          $align = [];
          foreach($styles as $style){
            if(array_key_exists($style, $tab_style_name)){
              array_push($name, $tab_style_name[$style]);
              array_push($family, $tab_style_family[$style]);
              array_push($font_weight, $tab_style_font_weight[$style]);
              array_push($font_style, $tab_style_font_style[$style]);
              array_push($align, $tab_style_align[$style]);
            } else {
              array_push($standard, $style);
            }
          }
          array_push($cases,genere_html($text, $standard, $name, $family, $font_weight, $font_style, $align));
        } elseif(preg_match('/^<text:list/', $td)) { //Liste
        }
      }
    }
    if(substr_count($squelette_table, '_-_') != count($cases)) {
      echo 'Erreur tableau';
    } else {
      $part_squelette = explode('_-_', $squelette_table);
      $res_table = '';
      for($n=0; $n<count($cases); $n++){
        $res_table = $res_table . $part_squelette[$n] . $cases[$n];
      }
      $res_table = $res_table . $part_squelette[$n];
      $fic = $fic . $res_table;
    }
  } else { //Autres balises non gérées
  }
}

$console = $console . "Parsing terminé <br>\n";

if ($fic[0] == "\n") {
  $fic = substr($fic, 1);
}


$titre = substr(explode('</h3>', $fic)[0], 4);
$fic = explode('<h3>' . $titre . '</h3>', $fic)[1];
$corps = str_replace('«', '«<em>', str_replace('»', '</em>»',$fic));
$corps = str_replace('> <', '><', $corps);
$corps = nettoie_balise_doublon($corps);
$corps = "<div id='corps_texte'>" . $corps . "</div>";
$titre = str_replace('«', '«<em>', str_replace('»', '</em>»',$titre));
if (creation_visuel($corps, $titre)) {
  $console = $console . "Création du visuel terminé<br>\n";
}
?>
<main id="corps">
  <?php
    echo '<input id="titre" type="text" value="' . $titre . '" placeholder="Titre de la page" />';
    echo '<textarea id="texte" type="textarea">' . $corps . '</textarea>';
  ?>
  <input id="slide" placeholder="Chemin de l'image pour le slide" type="text"/>
  <div id="icon">
    <img src="icon/copy.jpg" width="40px" id="copy" title="Copier le texte"/>
    <img src="icon/key_words.jpg" width="40px" id="key_words" title="Découper les mots-clés"/>
    <img src="icon/javascript.jpeg" width="40px" id="injection" title="Copier l'injection Javascript"/>
    <img src="icon/guttenberg.png" width="40px" id="guttenberg" title="Copier l'injection Javascript pour Guttenberg"/>
  </div>
</main>
<aside id="proprietes">
  <div id="date"><?php echo $date ?></div>
  <div id="console"><?php echo $console ?></div>
  <div id="ajouts">
    <input type="button" value="Ajouter version anglaise" onclick="ajouter_version('anglais');"/>
    <input type="button" value="Ajouter version allemande" onclick="ajouter_version('allemand');"/>
    <input type="button" value="Ajouter version catalane" onclick="ajouter_version('catalan');"/>
    <input type="button" value="Ajouter version espagnole" onclick="ajouter_version('espagnol');"/>
    <input type="button" value="Ajouter version portugaise" onclick="ajouter_version('portugais');"/>
    <input type="button" value="Ajouter version turque" onclick="ajouter_version('turk');"/>
    <div>
          <input type="button" class="short_button" value="Esperanto" onclick="ajouter_version('esperanto');"/>
          <input type="button" class="short_button" value="Italien" onclick="ajouter_version('italien');"/>
          <input type="button" class="short_button" value="Russe" onclick="ajouter_version('russe');"/>
    </div>
    <input type="button" value="Ajouter une autre version" onclick="ajouter_version('divers');"/>
    <hr>
    <input type="button" value="Ajouter un PDF" onclick="ajouter_pdf();"/>
    <hr>
    <input type="button" value="Ajouter balise more" onclick="ajouter_more()"/>
    <hr>
    <a href="visuel.html" target="_blank"><input type="button" value="Visualiser la page"/></a>
<!--    <hr>
    <a href="[URL_UPLOAD]" id="ftp" target="_blank"><input type="button" value="Uploader images/documents"/></a>
    <hr>
    <a href="download.php" target="_blank"><input type="button" value="Télécharger les images"/></a>-->
    <hr>
    <input type="button" value="Retour au choix du document" onclick="retour_accueil()"/>
  </div>
</aside>
<script src="js/script.js"></script>
<script src="js/edit.js"></script>
</body>
</html>
