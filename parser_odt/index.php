<DOCTYPE! HTML>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" type="text/css" href="css/accueil.css">
  <title>Accueil</title>
</head>
<body>
<?php

include_once 'tools_commun.php';

rmAllDir("temp");
rmAllDir("images");

if (!is_dir("temp")) {mkdir("temp");}
if (!is_dir("images")) {mkdir("images");}

$liste_files = scandir('/var/www/html/parser/parser_odt/document');
$nb_files = sizeof($liste_files);

echo"<div id='window'>\n
      <div id='dossier'>\n";
for ($i=0; $i < $nb_files; $i++){

  if (contient($liste_files[$i], '.odt') and !contient($liste_files[$i], '~')) { //seulement les docx et en ignorant les fichiers en edition ~NOM.docx
    echo '<div class="fichier" id="' .  $liste_files[$i] . '""><img src="icon/odt.png"  class="icon" /><p>' . explode('.odt', $liste_files[$i])[0] ."</p></div>\n";
  }
}
echo "</div>\n";
echo "</div>\n";

?>
<script src="js/accueil.js"></script>
</body>
</html>
