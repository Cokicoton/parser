<?php
function contient($string, $motif){
  $temp = $string . "?"; //au cas où le motif est en fin de chaine
  if (isset(explode($motif, $temp)[1])) {
    return true;
  } else {
    return false;
  }
}

function rmAllDir($strDirectory){
    $handle = opendir($strDirectory);
    while(false !== ($entry = readdir($handle))){
        if($entry != '.' && $entry != '..'){
            if(is_dir($strDirectory.'/'.$entry)){
                rmAllDir($strDirectory.'/'.$entry);
            }
            elseif(is_file($strDirectory.'/'.$entry)){
                unlink($strDirectory.'/'.$entry);
            }
        }
    }
    rmdir($strDirectory.'/'.$entry);
    closedir($handle);
}
?>
